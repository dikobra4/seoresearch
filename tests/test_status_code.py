import logging

import allure
import pytest

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class TestSmoke:

    tested_pages = 0

    @pytest.fixture(scope='class', autouse=True)
    def counter(self):
        self.tested_pages += 1
        logger.info(f'Тестируемая страница №{self.tested_pages}')

    @allure.title('Проверка статус кода')
    def test_status_code_200(self, response_and_url):
        logger.info(f'Проверка статус кода для url {response_and_url[0].url}')
        assert response_and_url[0].status_code == 200
        logger.info(f'Статус код - 200')

    @allure.title('Проверка h1 только одни на странице')
    def test_h1_tag_only_one(self, page, parsed_page):
        logger.info(f'Проверка наличия одного тега h1 на странице {page.current_url}')
        allure.attach(parsed_page.prettify(), attachment_type=allure.attachment_type.TEXT)
        h1_tags = parsed_page.findAll('h1')
        assert len(h1_tags) == 1

    @allure.title('Проверка h1 не пустой')
    def test_h1_tag_not_empty(self, page, parsed_page):
        logger.info(f'Проверка, что тег h1 не пустой')
        allure.attach(parsed_page.prettify(), attachment_type=allure.attachment_type.TEXT)
        h1_tag = parsed_page.select_one('h1')
        assert h1_tag.text != ''

    @allure.title('Проверка title не содержит url')
    def test_title_is_not_url(self, page, parsed_page):
        logger.info(f'Проверка, что в теге title не содержится url страницы')
        allure.attach(parsed_page.prettify(), attachment_type=allure.attachment_type.TEXT)
        assert 'http' not in parsed_page.select_one('title')
