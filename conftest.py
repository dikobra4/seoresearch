import time

import pytest
import logging
import requests
from bs4 import BeautifulSoup
from selenium import webdriver


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def get_sitemap_urls():
    page = requests.get('https://vrachu.ru/sitemap.xml')
    xml = BeautifulSoup(page.content, 'lxml-xml')
    url_tags = xml.findAll('url')
    urls = []
    for url_tag in url_tags:
        url = url_tag.find('loc').text
        urls.append(url)
    logger.info(f'Количество страниц для обхода {len(urls)}')
    return urls


@pytest.fixture(
    scope='class',
    params=get_sitemap_urls()
)
def response_and_url(request):
    response = requests.get(request.param)
    return response, request.param


@pytest.fixture(
    scope='class'
)
def page(response_and_url):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    options.add_experimental_option('excludeSwitches', ['enable-logging'])
    capabilities = {
            "browserName": "chrome",
            "browserVersion": "107.0",
            "selenoid:options": {
                "enableVNC": True,
                "enableVideo": False,
                "screenResolution": "1920x1080x24",
                "sessionTimeout": "120s"
            }
    }
    browser = webdriver.Remote(
            command_executor='http://localhost:4444/wd/hub',
            desired_capabilities=capabilities,
            options=options
        )
    browser.get(response_and_url[1])
    yield browser
    browser.quit()


@pytest.fixture(scope='class')
def parsed_page(page):
    parsed_page = BeautifulSoup(page.page_source)
    return parsed_page
